toolforge-ansible
=================

Usage looks like:

```
$ ansible-playbook -i hosts.yaml playbook.yaml
```

To update just one tool, add `--limit {tool}`.

## Roles

### tfbase

Initializes some variables for use with ansible in later roles,
needed by the other roles.

### tfrust

For tools written in Rust

### tflogrotate

Configures `logrotate` to daily rotate the `~/logs` directory.

## License

GPL v3, or at your option, any later version.
